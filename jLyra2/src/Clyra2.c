#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include "Clyra2.h"
#include "Lyra2.h"

JNIEXPORT jint JNICALL Java_Clyra2_LYRA2
  (JNIEnv *env, jobject thisObj,
  jbyteArray key_out, jlong key_out_len,
  jbyteArray usr_pswd, jlong usr_pswd_len,
  jbyteArray salt, jlong salt_len,
  jlong time_cost, jint mem_matrix_rows, jint mem_matrix_cols)
{
    const char* C_password = (*env)->GetByteArrayElements(env, usr_pswd, NULL);
    const char* C_salt = (*env)->GetByteArrayElements(env, salt, NULL);
    char* C_key = (char*) malloc(sizeof(char) * key_out_len);

    LYRA2(C_key, key_out_len,
        C_password, usr_pswd_len,
        C_salt, salt_len,
        time_cost, mem_matrix_rows, mem_matrix_cols);

    (*env)->SetByteArrayRegion(env, key_out, 0, key_out_len, C_key);
}

JNIEXPORT void JNICALL Java_Clyra2_initState
  (JNIEnv * env, jobject thisObj, jlongArray state)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    initState(C_state);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
}

JNIEXPORT void JNICALL Java_Clyra2_squeeze
  (JNIEnv *env, jobject thisObj, jlongArray state, jbyteArray in, jint len)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    char* C_in = (*env)->GetByteArrayElements(env, in, NULL);
    jsize in_len = len; //or the same: (*env)->GetArrayLength(env, in);

    squeeze(C_state, C_in, in_len);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetByteArrayRegion(env, in, 0, in_len, C_in);
}

JNIEXPORT void JNICALL Java_Clyra2_reducedSqueezeRow0
  (JNIEnv *env, jobject thisObj, jlongArray state, jlongArray row_out, jlong n_cols)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_row_out = (*env)->GetLongArrayElements(env, row_out, NULL);
    jsize row_out_len = (*env)->GetArrayLength(env, row_out);

    reducedSqueezeRow0(C_state, C_row_out, n_cols);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, row_out, 0, row_out_len, C_row_out);
}

JNIEXPORT void JNICALL Java_Clyra2_reducedBlake2bLyra
  (JNIEnv *env, jobject thisObj, jlongArray state)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    //reducedBlake2bLyra(C_state);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
}

JNIEXPORT void JNICALL Java_Clyra2_absorbBlock
  (JNIEnv *env, jobject thisObj, jlongArray state, jlongArray in)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_in = (*env)->GetLongArrayElements(env, in, NULL);
    jsize in_len = (*env)->GetArrayLength(env, in);

    absorbBlock(C_state, C_in);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, in, 0, in_len, C_in);
}

JNIEXPORT void JNICALL Java_Clyra2_absorbBlockBlake2Safe
  (JNIEnv *env, jobject thisObj, jlongArray state, jlongArray in)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_in = (*env)->GetLongArrayElements(env, in, NULL);
    jsize in_len = (*env)->GetArrayLength(env, in);

    absorbBlockBlake2Safe(C_state, C_in);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, in, 0, in_len, C_in);
}

JNIEXPORT void JNICALL Java_Clyra2_reducedDuplexRowSetup
  (JNIEnv *env, jobject thisObj, jlongArray state, jlongArray matrix, jint row_in, jint row_in_out, jint row_out, jlong n_cols)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_matrix = (*env)->GetLongArrayElements(env, matrix, NULL);
    jsize matrix_len = (*env)->GetArrayLength(env, matrix);

    int64_t* C_row_in = C_matrix + row_in;
    int64_t* C_row_out = C_matrix + row_out;
    int64_t* C_row_in_out = C_matrix + row_in_out;

    reducedDuplexRowSetup(C_state, C_row_in, C_row_in_out, C_row_out, n_cols);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, matrix, 0, matrix_len, C_matrix);
}

JNIEXPORT void JNICALL Java_Clyra2_reducedDuplexRow
  (JNIEnv *env, jobject thisObj, jlongArray state, jlongArray matrix, jint row_in, jint row_in_out, jint row_out, jlong n_cols)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_matrix = (*env)->GetLongArrayElements(env, matrix, NULL);
    jsize matrix_len = (*env)->GetArrayLength(env, matrix);

    int64_t* C_row_in = C_matrix + row_in;
    int64_t* C_row_out = C_matrix + row_out;
    int64_t* C_row_in_out = C_matrix + row_in_out;

    reducedDuplexRow(C_state, C_row_in, C_row_in_out, C_row_out, n_cols);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, matrix, 0, matrix_len, C_matrix);
}

JNIEXPORT void JNICALL Java_Clyra2_reducedDuplexRow1
  (JNIEnv *env, jobject thisObj, jlongArray state,jlongArray matrix, jint row_in, jint row_out, jlong n_cols)
{
    int64_t* C_state = (*env)->GetLongArrayElements(env, state, NULL);
    jsize state_len = (*env)->GetArrayLength(env, state);

    int64_t* C_matrix = (*env)->GetLongArrayElements(env, matrix, NULL);
    jsize matrix_len = (*env)->GetArrayLength(env, matrix);

    int64_t* C_row_in = C_matrix + row_in;
    int64_t* C_row_out = C_matrix + row_out;

    reducedDuplexRow1(C_state, C_row_in, C_row_out, n_cols);

    (*env)->SetLongArrayRegion(env, state, 0, state_len, C_state);
    (*env)->SetLongArrayRegion(env, matrix, 0, matrix_len, C_matrix);
}

JNIEXPORT jint JNICALL Java_Clyra2_padding
  (JNIEnv *env, jobject thisObj, jint kLen, jint timeCost, jbyteArray pwd, jbyteArray salt, jint nRows, jint nCols, jlongArray wholeMatrix)
{
    int64_t* C_wholeMatrix = (*env)->GetLongArrayElements(env, wholeMatrix, NULL);
    uint64_t wholeMatrix_len = (*env)->GetArrayLength(env, wholeMatrix);

    char* C_pwd = (*env)->GetByteArrayElements(env, pwd, NULL);
    uint64_t pwdlen = (*env)->GetArrayLength(env, pwd);

    char* C_salt = (*env)->GetByteArrayElements(env, salt, NULL);
    uint64_t saltlen = (*env)->GetArrayLength(env, salt);

    //padding(kLen, timeCost, nRows, nCols, C_pwd, pwdlen, C_salt, saltlen, C_wholeMatrix);

    (*env)->SetLongArrayRegion(env, wholeMatrix, 0, wholeMatrix_len, C_wholeMatrix);
    (*env)->SetByteArrayRegion(env, pwd, 0, pwdlen, C_pwd);
    (*env)->SetByteArrayRegion(env, salt, 0, saltlen, C_salt);
}
